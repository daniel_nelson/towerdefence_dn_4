﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GlobalAmmo : MonoBehaviour {
	public static int currentAmmo;
	public static int currentExtraAmmo;
	public static int maxClipSize = 10;
	public static int maxExtraAmmo = 20;
	int internalAmmo;
	public GameObject loadedAmmoDisplay;
	public GameObject extraAmmoDisplay;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		//internalAmmo = currentAmmo;
		loadedAmmoDisplay.GetComponent<Text> ().text = "" + currentAmmo;
		extraAmmoDisplay.GetComponent<Text> ().text = "/ " + currentExtraAmmo;
	}
}
