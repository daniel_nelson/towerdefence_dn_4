﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BasicEnemy : MonoBehaviour {
	float moveSpeed, maxHealth, health;
	Grid grid;
	//armor,regen,other special properties, targets,end bases,tower lists,	players, attack method, switch ai
	float weaponDamage, weaponRange;
	Pathfinder pathfinder;
	public enum targetStateEnum{targetBase,targetPlayer,targetTurret,attackingBase}
	targetStateEnum state = targetStateEnum.targetBase;
	int currentPathIndex = 0;
	List<Vector2> path;
	public Vector3 moveTarget;
	List<GameObject> players;
	GameObject endBase;
	Vector3 playerPos;
	// Use this for initialization
	void Start () {
		players = new List<GameObject>(GameObject.FindGameObjectsWithTag ("Player"));
		grid = GameObject.Find("A*").GetComponent<Grid>();
		pathfinder = new Pathfinder(grid);
		endBase = GameObject.Find ("EndBase");
		playerPos = players [0].transform.position;
		updateState ();
		updatePath ();
	}
	
	// Update is called once per frame
	void Update () {
		updateState ();
		stateActions ();
		//if player moves or (add later if a tower is placed down) update path
		if (playerPos != players [0].transform.position) {
			updatePath ();
		}
		followPath ();
		playerPos = players [0].transform.position;
	}

	void updateState(){
		if (state != targetStateEnum.targetPlayer) {
			if (state != targetStateEnum.attackingBase && Vector3.Distance (endBase.transform.position, transform.position) < weaponRange) {
				state = targetStateEnum.attackingBase;
			} else {
				state = targetStateEnum.targetBase;
			}
		}
	}
	void stateActions(){
		switch (state) {
		case targetStateEnum.targetBase:
			break;
		case targetStateEnum.attackingBase:
			Explode ();
			break;
		}
	}
	void Explode() {
		var exp = GetComponent<ParticleSystem>();
		exp.Play();
		Destroy(gameObject, exp.duration);
	}
	public void setAtributes(float moveSpeed, float health, float weaponDamage, float weaponRange, targetStateEnum e){
		this.weaponRange = weaponRange;
		this.moveSpeed = moveSpeed;
		this.health = maxHealth = health;
		this.weaponDamage = weaponDamage;
		state = e;
		Start ();
		updatePath();
		calculateMoveTarget();
	} 
	//when this enemy takes damage returns true if it died 
	public bool damagingHit(float hitAmount){
		health -= hitAmount;
		if(health <= 0){
			deathActions();
			return true;
		}
		return false;
	}
	private void deathActions(){
		//stuff

		Destroy (gameObject);
	}
	private void followPath(){
		if(path != null && currentPathIndex < path.Count){
			if(moveTarget != transform.position){
				Debug.Log ("DOPG");
				calculateMoveTarget();
			}
			else{
				Debug.Log ("DOPG1");
				currentPathIndex++;
				//possible overflow error
				calculateMoveTarget();
			}
			float step = moveSpeed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, moveTarget, step);
		}
	}
	private void calculateMoveTarget(){
		moveTarget = grid.GetWorldPos((int)path[currentPathIndex].x, (int)path[currentPathIndex].y);
		moveTarget = new Vector3(moveTarget.x, transform.position.y, moveTarget.z);
		Debug.Log ("pos: " + transform.position + "mov: "+moveTarget + "step: " + moveSpeed + "hp: "+health);
	}
	public void updatePath(){
		switch(state){
		case targetStateEnum.targetPlayer:
			updatePathP (players);
			break;
		case targetStateEnum.targetBase:
		default:
			updatePath (endBase);
			break;
		}
	}
	private void updatePath(GameObject target){
		path = pathfinder.findPath (grid.GetVector2 (transform.position), grid.GetVector2 (target.transform.position));
		currentPathIndex = 0;
	}
	//calculate best player to target
	//overide this method for smarter behievour
	private void updatePathP(List<GameObject> targets){		Debug.Log ("just: " + players[0].transform.position);
		
		updatePath (targets [0]);

	}
}
