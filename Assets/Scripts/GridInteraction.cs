﻿using UnityEngine;
using System.Collections;

public class GridInteraction : MonoBehaviour {

	GameObject wireBox;
	Grid grid;
	public Material wireBoxMat;
	GameObject currentBox;
	GridNode currentNode;
	public bool gridInteractionEnabled;
	// Use this for initialization
	void Start () {
		grid = GetComponent<Grid> ();
		gridInteractionEnabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.I)) {
			gridInteractionEnabled = !gridInteractionEnabled;
			Destroy (currentBox);
			Destroy (wireBox);
		}
		if (gridInteractionEnabled) {
			var mainCamera = FindCamera ();

			// We need to actually hit an object
			RaycastHit hit = new RaycastHit ();
			if (
				!Physics.Raycast (mainCamera.ScreenPointToRay (Input.mousePosition).origin,
					mainCamera.ScreenPointToRay (Input.mousePosition).direction, out hit, 4,
					Physics.DefaultRaycastLayers)) {
				Destroy (currentBox);
				Destroy (wireBox);
				return;
			}
			// Check that we are hitting a gameobject with tag Ground
			if (hit.collider.gameObject.tag != "Ground") {
				Destroy (currentBox);
				Destroy (wireBox);
				return;
			}
			//If inside grid
			if (grid.GetNode (hit.point).worldPos != new Vector3 (0, Mathf.Infinity, 0)) {
				//check if we have got to this point before
				if (currentNode != null) {
					//if we have then check if last time we got to this point we were pointing at the same node
					if (grid.GetNode (hit.point).worldPos == currentNode.worldPos) {
						//if we were return
						return;
					}
				}
				//else destroy the previous box and instantiate a new one at the new node
				Destroy (currentBox);
				Destroy (wireBox);
				currentNode = grid.GetNode (hit.point);
				Vector3 instanPos = currentNode.worldPos;
				wireBox = GameObject.CreatePrimitive (PrimitiveType.Cube);
				wireBox.transform.localScale = new Vector3 (grid.nodeRadius * 2, grid.nodeRadius / 2, grid.nodeRadius * 2);
				wireBox.GetComponent<MeshRenderer> ().material = wireBoxMat;
				wireBox.GetComponent<BoxCollider> ().enabled = false;
				currentBox = (GameObject)Instantiate (wireBox, instanPos, transform.rotation);
			}
		}
	}
	private Camera FindCamera()
	{
		if (GetComponent<Camera>())
		{
			return GetComponent<Camera>();
		}

		return Camera.main;
	}
}
