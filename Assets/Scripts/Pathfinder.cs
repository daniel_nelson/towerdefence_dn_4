﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//class adds pathfinding
//initializeSearchNodes creates the node grid and should be recalled if the map changes
//find path returns the optimal path from vector 1 to vector 2
public class Pathfinder : MonoBehaviour
{
	//holds the nodes to pathfind through
	private SearchNode[,] searchNodes;
	//width and height of grids in the map
	private int levelWidth, levelHeight;
	// Holds search nodes that are avaliable to search.
	private List<SearchNode> openList = new List<SearchNode> ();
	// Holds the nodes that have already been searched.
	private List<SearchNode> closedList = new List<SearchNode> ();

	public Pathfinder (Grid grid)
	{
		initializeSearchNodes (grid);
	}

	public void initializeSearchNodes (Grid grid)
	{
		levelWidth = grid.gridSizeX;
		levelHeight = grid.gridSizeY;
		//creates and initualises the search nodes
		searchNodes = new SearchNode[levelWidth, levelHeight];
		for (int x = 0; x < levelWidth; x++) {
			for (int y = 0; y < levelHeight; y++) {
				SearchNode node = new SearchNode ();
				node.position = new Vector2 (x, y);
				node.walkable = grid.GetWalkable (x, y);
				node.moveCost = 1;
				if (node.walkable == true) {
					node.neighbors = new SearchNode[4];
					searchNodes [x, y] = node;
				}
			}
		}
		//initialise the neighbor links 
		for (int x = 0; x < levelWidth; x++) {
			for (int y = 0; y < levelHeight; y++) {
				SearchNode node = searchNodes [x, y];
				if (node == null || node.walkable == false) {
					continue;
				}
				Vector2[] neighbors = new Vector2[] {
					new Vector2 (x, y - 1), // The node above the current node
					new Vector2 (x, y + 1), // The node below the current node.
					new Vector2 (x - 1, y), // The node left of the current node.
					new Vector2 (x + 1, y), // The node right of the current node
				};
				for (int i = 0; i < neighbors.Length; i++) {
					Vector2 position = neighbors [i];
					if (position.x < 0 || position.x > levelWidth - 1 || position.y < 0 || position.y > levelHeight - 1) {
						continue;
					}
					SearchNode neighbor = searchNodes [(int)position.x, (int)position.y];
					if (neighbor == null || neighbor.walkable == false) {
						continue;
					}
					node.neighbors [i] = neighbor;
				}
			}
		}
	}
	//return the manhattan distance distance between two vectors
	private float heuristic (Vector2 v1, Vector2 v2)
	{
		return Mathf.Abs (v1.x - v2.x) +
			Mathf.Abs (v1.y - v2.y);
	}

	//resets the search nodes
	private void resetSearchNodes ()
	{
		openList.Clear ();
		closedList.Clear ();
		for (int x = 0; x < levelWidth; x++) {
			for (int y = 0; y < levelHeight; y++) {
				SearchNode node = searchNodes [x, y];
				if (node == null) {
					continue;
				}
				node.inOpenList = false;
				node.inClosedList = false;
				node.distanceTraveled = float.MaxValue;
				node.distanceToGoal = float.MaxValue;
			}
		}
	}
	// Returns the node with the smallest distance to goal.
	private SearchNode findBestNode ()
	{
		SearchNode currentTile = openList [0];
		float smallestDistanceToGoal = float.MaxValue;
		// Find the closest node to the goal.
		for (int i = 0; i < openList.Count; i++) {
			if (openList [i].distanceToGoal < smallestDistanceToGoal) {
				currentTile = openList [i];
				smallestDistanceToGoal = currentTile.distanceToGoal;
			}
		}
		return currentTile;
	}
	// Use the parent field of the search nodes to trace
	// a path from the end node to the start node.
	private List<Vector2> findFinalPath (SearchNode startNode, SearchNode endNode)
	{
		closedList.Add (endNode);
		SearchNode parentTile = endNode.parent;
		// Trace back through the nodes using the parent fields
		// to find the best path.
		while (parentTile != startNode) {
			closedList.Add (parentTile);
			parentTile = parentTile.parent;
		}
		List<Vector2> finalPath = new List<Vector2> ();
		// Reverse the path
		for (int i = closedList.Count - 1; i >= 0; i--) {
			finalPath.Add (new Vector2 (closedList [i].position.x, closedList [i].position.y));
		}
		return finalPath;
	}
	// Finds the optimal path from one point to another.
	public List<Vector2> findPath (Vector2 startPoint, Vector2 endPoint)
	{
		// Only try to find a path if the start and end points are different.
		if (startPoint == endPoint) {
			return new List<Vector2> ();
		}

		/////////////////////////////////////////////////////////////////////
		// Step 1 : Clear the Open and Closed Lists and reset each node’s F 
		//          and G values in case they are still set from the last 
		//          time we tried to find a path. 
		/////////////////////////////////////////////////////////////////////
		resetSearchNodes ();

		// Store references to the start and end nodes for convenience.
		SearchNode startNode = searchNodes [(int)startPoint.x, (int)startPoint.y];
		SearchNode endNode = searchNodes [(int)endPoint.x, (int)endPoint.y];

		/////////////////////////////////////////////////////////////////////
		// Step 2 : Set the start node’s G value to 0 and its F value to the 
		//          estimated distance between the start node and goal node 
		//          (this is where our H function comes in) and add it to the 
		//          Open List. 
		/////////////////////////////////////////////////////////////////////
		startNode.inOpenList = true;

		startNode.distanceToGoal = heuristic (startPoint, endPoint);
		startNode.distanceTraveled = 0;

		openList.Add (startNode);

		/////////////////////////////////////////////////////////////////////
		// Setp 3 : While there are still nodes to look at in the Open list : 
		/////////////////////////////////////////////////////////////////////
		while (openList.Count > 0) {
			/////////////////////////////////////////////////////////////////
			// a) : Loop through the Open List and find the node that 
			//      has the smallest F value.
			/////////////////////////////////////////////////////////////////
			SearchNode currentNode = findBestNode ();

			/////////////////////////////////////////////////////////////////
			// b) : If the Open List empty or no node can be found, 
			//      no path can be found so the algorithm terminates.
			/////////////////////////////////////////////////////////////////
			if (currentNode == null) {
				break;
			}

			/////////////////////////////////////////////////////////////////
			// c) : If the Active Node is the goal node, we will 
			//      find and return the final path.
			/////////////////////////////////////////////////////////////////
			if (currentNode == endNode) {
				// Trace our path back to the start.
				return findFinalPath (startNode, endNode);
			}

			/////////////////////////////////////////////////////////////////
			// d) : Else, for each of the Active Node’s neighbours :
			/////////////////////////////////////////////////////////////////
			for (int i = 0; i < currentNode.neighbors.Length; i++) {
				SearchNode neighbor = currentNode.neighbors [i];

				//////////////////////////////////////////////////
				// i) : Make sure that the neighbouring node can 
				//      be walked across. 
				//////////////////////////////////////////////////
				if (neighbor == null || neighbor.walkable == false) {
					continue;
				}

				//////////////////////////////////////////////////
				// ii) Calculate a new G value for the neighbouring node.
				//////////////////////////////////////////////////
				float distanceTraveled = currentNode.distanceTraveled + currentNode.moveCost;

				// An estimate of the distance from this node to the end node.
				float heuristicVal = heuristic (neighbor.position, endPoint);

				//////////////////////////////////////////////////
				// iii) If the neighbouring node is not in either the Open 
				//      List or the Closed List : 
				//////////////////////////////////////////////////
				if (neighbor.inOpenList == false && neighbor.inClosedList == false) {
					// (1) Set the neighbouring node’s G value to the G value 
					//     we just calculated.
					neighbor.distanceTraveled = distanceTraveled;
					// (2) Set the neighbouring node’s F value to the new G value + 
					//     the estimated distance between the neighbouring node and
					//     goal node.
					neighbor.distanceToGoal = distanceTraveled + heuristicVal;
					// (3) Set the neighbouring node’s Parent property to point at the Active 
					//     Node.
					neighbor.parent = currentNode;
					// (4) Add the neighbouring node to the Open List.
					neighbor.inOpenList = true;
					openList.Add (neighbor);
				}
				//////////////////////////////////////////////////
				// iv) Else if the neighbouring node is in either the Open 
				//     List or the Closed List :
				//////////////////////////////////////////////////
				else if (neighbor.inOpenList || neighbor.inClosedList) {
					// (1) If our new G value is less than the neighbouring 
					//     node’s G value, we basically do exactly the same 
					//     steps as if the nodes are not in the Open and 
					//     Closed Lists except we do not need to add this node 
					//     the Open List again.
					if (neighbor.distanceTraveled > distanceTraveled) {
						neighbor.distanceTraveled = distanceTraveled;
						neighbor.distanceToGoal = distanceTraveled + heuristicVal;

						neighbor.parent = currentNode;
					}
				}
			}

			/////////////////////////////////////////////////////////////////
			// e) Remove the Active Node from the Open List and add it to the 
			//    Closed List
			/////////////////////////////////////////////////////////////////
			openList.Remove (currentNode);
			currentNode.inClosedList = true;
		}

		// No path could be found.
		return new List<Vector2> ();
	}

	//nodes that store pathfinding information
	private class SearchNode
	{
		public Vector2 position;
		public bool walkable, inOpenList, inClosedList;
		public SearchNode[] neighbors;
		public SearchNode parent;
		public float distanceToGoal, distanceTraveled, moveCost;
	}
}