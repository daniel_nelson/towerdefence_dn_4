﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class EnemyScript : MonoBehaviour
{
    int health = 10;
    public Grid grid;
    public GameObject player;
	public GameObject endBase;
	private Collider targetTower;
    List<Vector2> path;
    Pathfinder pathfinder;
    int currentPathIndex;
    Rigidbody rb;
    Vector3 V3null = new Vector3(0, 99999999, 0);
    Vector3 target;
    public float speed = 5;
	public string state = "BaseTarget";
    // Use this for initialization
    void Start()
    {
		player = GameObject.Find("FPSController");
		endBase = GameObject.Find("EndBase");
        grid = GameObject.Find("A*").GetComponent<Grid>();
        pathfinder = new Pathfinder(grid);
        rb = GetComponent<Rigidbody>();
        path = new List<Vector2>();
        UpdatePath();
   		target = V3null;	

    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            Destroy(gameObject);
        }


        Move();
        UpdatePath();
    }
    void Move()
    {
        if (target == V3null)
        {
            Debug.Log("Target: " + target + "  Enemy: " + transform.position);
            target = grid.GetWorldPos((int)path[currentPathIndex].x, (int)path[currentPathIndex].y);
            target = new Vector3(target.x, transform.position.y, target.z);
        }
        else if (target == transform.position)
        {
            Debug.Log("11Target: " + target + "  Enemy: " + transform.position);
            currentPathIndex++;
            target = grid.GetWorldPos((int)path[currentPathIndex].x, (int)path[currentPathIndex].y);
            target = new Vector3(target.x, transform.position.y, target.z);
        }

        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target, step);

    }

    public void UpdatePath()
    {
		if (state.Equals ("PlayerTarget")) {
			Debug.Log ("Vec2: " + grid.GetVector2 (transform.position) + " Play" + grid.GetVector2 (player.transform.position));
			path = pathfinder.findPath (grid.GetVector2 (transform.position), grid.GetVector2 (player.transform.position));
			currentPathIndex = 0;
			target = V3null;
		} else if (state.Equals ("BaseTarget")) {
			Debug.Log ("Vec2: " + grid.GetVector2 (transform.position) + " Play" + grid.GetVector2 (endBase.transform.position));
			path = pathfinder.findPath (grid.GetVector2 (transform.position), grid.GetVector2 (endBase.transform.position));
			currentPathIndex = 0;
			target = V3null;
		} else if (state.Equals ("TowerTarget")) {
			Debug.Log ("Vec2: " + grid.GetVector2 (transform.position) + " Play" + grid.GetVector2 (targetTower.transform.position));
			path = pathfinder.findPath (grid.GetVector2 (transform.position), grid.GetVector2 (targetTower.transform.position));
			currentPathIndex = 0;
			target = V3null;
		}
    }
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Tower")
		{
			state = "TowerTarget";
			targetTower = other;
			UpdatePath ();
		}
	}
		
    void RemoveHealth(int damage)
    {
        health -= damage;
    }
}
